AgoraExMachina - Plateforme de démocratie liquide - V2
==========================================================

Présentation générale
------------------------
**AGORA EX MACHINA** (**AEM**) est une plateforme `web` de démocratie liquide fonctionnant sous PHP/MySQL s'inspirant initialement de la solution allemande `Adhocracy` (https://github.com/liqd/adhocracy).

La démocratie liquide est un principe démocratique dans lequel une `démocratie participative` est implémentée par une part de `démocratie représentative` : chaque vote peut être délégué à une autre personne dont on considère l’expertise. Dans une plateforme web, les mécanismes qui permettent et facilitent l’expression le `Vote` et sa `Délégation` sont fluidifiées, facilitées et rendues plus rapides.

Présentation de la plateforme
-----------------------------
La plateforme **AEM** permet de créer des `Ateliers` organisés par `Thème`. Les `Utilisateurs` sont regroupés en `Cohortes`. Tout membre inscrit dans une `Cohorte` appelé `Participant` peut participer à l'ensemble des `Thèmes`, `Ateliers`, `Propositions`, `Discussions` et `Voter` des `Propositions`. Il peut également choisir de `Déléguer` son vote à une autre personne de son choix inscrite à la`Cohorte`. 

Les `Ateliers` sont conçus pour que les phases de discussion et de vote puissent être dissociées : ainsi, un groupe de `Participants` peut, dans un premier temps discuter sur un sujet précis puis, dans un second, `Voter`. Une fois l'atelier terminé, les votes sont recueillis et des calculs de participation au vote, d'acceptation du vote au regard du *quorum*, etc. sont présentés au public. Les `Propositions` ayant recueillies le plus de votes (qu'ils soient positifs, négatifs ou neutres) sont validées à l'issue de l'atelier.

Plusieurs types de `Votes` sont proposés :
* Un `Vote` simple permet de choisir entre Oui, Non ou Abstention pour chacune des `Propositions`.
* Un `Vote` à cinq choix (Tout à fait d'Accord, d'Accord, À peu près d'Accord, Pas d'accord, Pas du tout d'Accord)
* Un `Vote` à points qui permet au `Participant` de les distribuer à sa convenance entre l'ensemble des `Propositions` à voter.

Avis aux développeurs : d'autres votes (Borda, jugement majoritaire, ...) sont les bienvenus.

What's new ?
-------------------------------------------------
La version présentée ici est en développement. Elle vise à déplacer la grande majorité des fonctionnalités de la [V1](https://gitlab.com/adenum/aem-agoraexmachina) sous forme de bundles (micro-services) développables indépendamment du Core.

Présentation technique
---------------------------

Languages utilisés
*********************
* HTML5
* CSS 3
* PHP 7.4
* MySql 5
* Javascript


Outils utilisés
*********************
* Bootstrap
* Jquery
* Framework [Symphony](https://symfony.com/ "lien vers symphony")
 

Installation
-----------------
Lire le fichier `INSTALL.md`

Lors de la première connexion sur la plateforme à l'adresse `http://mon.site/public/login`, vous devez vous connecter en tant qu'administrateur (nom d'utilisateur par défaut "admin@mail.com" et mot de passe par défaut "Password0@") et créer vos premières catégories. Les catégories regrouperont vos ateliers de discussions et de votes.

Pour aider au développement d'**AEM**
------------------------------------------------

Un cahier des charges fonctionnel complété au fur et à mesure est mis à votre disposition dans le dossier docs.

