# Installation de la plateforme de démocratie liquide Agora Ex Machina (AEM)
## Copie des fichiers
### Si vous n’êtes pas familier avec un repository git
* Télécharger et décompresser l’archive en local
* Ajouter les infos dans le fichier `.env`, en fin de fichier, ajoutez les informations relatives au serveur de bases de données (adresse, identifiant, mot de passe, nom de la base de données) selon le modèle prévu à cet effet. La base de données par défaut est au format `PostGreSQL` : décommentez la ligne correspondant au type de base de données que vous désirez utiliser et remplissez les informations.
* Copiez l’ensemble des fichiers dans un répertoire nommé agoraexmachina

Notez que le système mail sera finalisé dans les mois qui viennent.
### Si vous êtes familier avec un repository git
* Utilisez `git clone` pour télécharger l'ensemble des fichiers nécessaires à  l'installation. 
## Installation
### Préambule
**AgoraExMachina** est développé à l'aide du Framework Symfony. Il est nécessaire :
* soit d'installer le gestionnaire de paquets `Composer` à votre serveur php/MySQL
* soit d'utiliser `composer.phar` pour permettre d'acquisition des paquets.

### Avec composer

* `composer install --no-dev --optimize-autoloader`
* `composer require symfony/dotenv`
* Editer le fichier `.env` pour indiquer l'adresse de la base de données. 
* `php bin/console doctrine:database:create` (uniquement si elle n'a pas été créée auparavant).
* `php bin/console doctrine:migrations:execute --up 1`
### Avec composer.phar
* `php composer.phar install --no-dev --optimize-autoloader` dans le répertoire agoraexmachina
* `composer require symfony/dotenv`
* Editer le fichier `.env` pour indiquer l'adresse de la base de données. 
* `php bin/console doctrine:database:create` (uniquement si elle n'a pas été créée auparavant).
* `php bin/console doctrine:migrations:execute --up 1`
### Ubuntu
Dans un configuration générale en local sous Ubuntu, il peut ariver que des problèmes de droits se produisent. Si la méthode Composer décrite plus haut ne fonctionne pas, préférer :

* `sudo composer install --no-dev --optimize-autoloader`
* `sudo composer require symfony/dotenv `
* Editer le fichier `.env` pour indiquer l'adresse de la base de données comme indiqué ci-dessus. 
* `sudo php bin/console doctrine:database:create`  (uniquement si elle n'a pas été créée auparavant).
* `sudo php bin/console doctrine:migrations:execute --up 1`
* `sudo php bin/console doctrine:migrations:execute --up 20211025043526`
* `sudo php bin/console doctrine:migrations:execute --up 20211005044940`
* `sudo chmod 777 public/img/upload`
* `sudo chmod 777 public/pdf/upload`
### Procédure post-installation
* Dans un navigateur, se placer dans l’interface d’administration de AEM (http://mondomaine.com/agoraexmachina/public)
* S'enregistrer en admin (admin@mail.com / Password0@)
* Pour configurer le mailer, il faut l'adapter dans le fichier .env. Pour plus d'informations,  [Consultez l'aide de Symfony](https://symfony.com/doc/current/mailer.html).

Si les problèmes persistent, il faut changer le regex utilisé pour récupérer l'email dans la fonction sendEmailToUser dans l'entité User

### Et ensuite
* Il est possible de modifier un certain nombre de variables dans le fichier `config/services.yaml`. parmi celles-ci :
  * le nom de la plateforme (affiché dans le footer)
  * Le langage, la lang et l'écriture (pour certaines balises, voir dans le fichier `templates/base.html.twig` pour plus de détails).
## Remarques
La solution de démocratie liquide est en développement. Pour le mettre en version de production, remplacez la variable ad hoc dans le fichier `.env`. 

Pour tout bug relatif à l'installation, veuillez vous adresser aux auteurs : 
* Cyril Bazin : crlbazin@gmail.com
* Valentin Trouillet : trouillet.valentin@gmail.com
* François Mauviard : info@mobile-adenum.fr
* Keyttlin Malicieux : keyttlin.m@hotmail.com.

Le projet **AgoraExMachina** est défendu et accompagné avec le soutien de l'association **ADENÜM**.
