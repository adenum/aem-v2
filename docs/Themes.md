# Documentation des thèmes
******
##Sommaire:

<ul>

### [Création d'un thème](#create-theme)

<ol style="list-style-type: upper-alpha">
<li>

#### [Un thème a deux formes :](#theme-two-shape)

</li>

  <ol style="list-style-type: lower-roman">
  <li>
  
  #### [Forme 1 :](#shape-one)

</li>
<li>  

#### [Forme 2 :](#shape-two)

</li>

  </ol>

<li>

#### [Explication du contenu du fichier config.json :](#explanation-config-json)

<ol style="list-style-type: lower-roman">
<li>

#### [Fondamentaux](#fundamentals)

</li>
<li>

#### [Facultatif](#optional)

</li>

</ol>

</li>

</ol>

### [Installation d'un thème](#theme-install)
</ul>


##<a id="create-theme"/> Création d'un thème


<ol type="A">




###<a id="theme-two-shape"/> <li><u>Un thème a deux formes :</u></li>


<ol type="I">

###<a id="shape-one"/><u><li>Forme 1 :</li></u>

Modifie l'apparence du site en modifiant seulement le css.

<u>**Arborescence du fichier :**</u>

    RépertoireSource
    ├── css
    │   ├── animate.css
    │   ├── bootstrap.min.css
    │   ├── classic.bootstrap.min.css 
    │   ├── jquery.mCustomScrollbar.min.css
    │   ├── main.css
    │   ├── media-queries.css
    │   ├── menu.css
    │   ├── signin.css
    │   └── style.css 
    ├── config.json
    └── icon.png 

<u>**Spécificité :**</u>

* **RépertoireSource :**
  * Correspond au nom du thème.
  * Il doit correspondre au prefix contenu dans le fichier config.json.
  
  <br/>
* **CSS :**
  * Répertoire contenant les fichiers css à surcharger.
  * Les noms de fichier présentés dans l'arborescence ci-dessus doivent être respectés.
  * Le nom du répertoire ne doit pas être modifié.
  
  <br/>
* **config.json :**
  * Contient toutes les informations sur le thème.
  * Le nom de ce fichier ne doit pas être modifié.

  <br/>
* **icon.png :**
  * Image donnant un aperçu succinct du thème.
  * Le nom de ce fichier ne doit pas être modifié.

  <br/>
* **Général :**
  * Image donnant un aperçu succinct du thème.

  <br/>

###<a id="shape-two"/><u><li>Forme 2 :</li></u>

Modifie l'apparence et la structure des pages du site en modifiant à la fois le css et les modèles.

<u>**Arborescence du fichier :**</u>

    RépertoireSource
    ├── public
    │   ├── css
    │   │   └── tousLesFichiersCssNécessaires.css
    │   ├── fonts
    │   │   └── tousLesFichiersFontsNécessaires.ttf
    │   ├── img
    │   │   └── tousLesFichiersImagesNécessaires.jpg
    │   └── js
    │       └── tousLesFichiersJavaScriptNécessaires.js
    ├── templates
    │   ├── repertoireTwig
    │   │   └── FichierTwig.html.twig
    │   └── FichierTwig.html.twig
    ├── translations
    │   ├── messages.en.xlf
    │   └── messages.langue.xlf
    ├── config.json
    └── icon.png 

<u>**Spécificité :**</u>


* **RépertoireSource :**
    * Correspond au nom du thème.
    * Il doit correspondre au prefix contenu dans le fichier config.json.

  <br/>
* **Public :**
  * Contient tous les fichiers publics du site web.
  * Ce répertoire est facultatif. (Vous pouvez utiliser le dossier "public" déjà présent du site web).
  * Le nom du répertoire ne doit pas être modifié.

  <br/>
* **Templates:**
  * Tous les fichiers twig par défaut ne sont pas obligatoirement à surcharger.
  * Les fichiers twig que l'on souhaite surcharger doivent respecter, pour leur nom et leur répertoire, 
    le même nommage que dans le thème par défaut.
  * De nouveaux fichiers twig servant à être hérités peuvent être ajoutés.

  <br/>
  
* **Translations :**
  * Contient tous les fichiers de translations au format xlf.
  * Ce répertoire est facultatif. (Vous pouvez utiliser le dossier "translations" déjà présent du site web).
  * Le nom du répertoire ne doit pas être modifié.

  <br/>
* **config.json :**
    * Contient toutes les informations sur le thème.

  <br/>
* **icon.png :**
    * Image donnant un aperçu succinct du thème.
    
<br>
</ol>

###<a id="explanation-config-json"/> <li><u>Explication du contenu du fichier config.json :</u></li>


    {
        "prefix": "default",
        "version": "2.0.0",
        "etat": "stable",
        "schema": 2,
        "compatibilite": "[3.0.0;3.2.*]",
        "documentation": "default.agora-ex-machina.com",
        "credit":  "null",
        "licence": "null",
        "utilise": "null",
        "nom": "Default",
        "auteurs": 
            {
            "0":   
                {
                "name": "auteur1",
                "email": "auteur1@adenum.fr",
                "site": "auteur1-agora-ex-machina.fr"
                },
            "1" : 
                {
                "name": "auteur2",
                "email": "auteur2@adenum.com",
                "site": "auteur2-agora-ex-machina.com"
                }
            }
    }
<ol type="I">

###<a id="fundamentals"/> <u><li>Fondamentaux</li></u>

**prefix :**
* Identifiant qui doit être unique entre tous les thèmes.
* Correspond au nom du répertoire contenant les fichiers du thème ("RépertoireSource" dans les arborescences ci-dessus).

**version :**
* Correspond à la version du thème en question.

**etat :**
* Correspond à l'état global du thème.
  * stable : la version est terminée.
  * dev : le thème est en cours de développement. (Des soucis peuvent survenir en cas d'utilisation).

**schema :**
* Cet attribut a deux valeurs possibles :
    * 1 : [Correspond à la forme 1 du document](#formeun)
    * 2 : [Correspond à la forme 2 du document](#formedeux)

###<a id="optional" /> <u><li>Facultatif</li></u>
**compatibilite :**
* Version du site avec lequel le thème est compatible.

**documentation :**
* Lien vers la documentation du thème.

**credit :**
* .

**licence :**
* .

**nom :**
* Nom du thème affiché sur le site

**auteurs :**
* Liste du/des auteur(s) du thème:
  * **name** : nom de l'auteur
  * **email** : email de l'auteur
  * **site** : site de l'auteur


</ol>
</ol>

***

##<a id="theme-install"/> Installation d'un thème

Télécharger les fichiers de votre thème sur internet, copier les dans le répertoire "themes" se trouvant à la racine 
de votre projet.

Vérifier que le nom du répertoire dans lequel se trouvent les fichiers de votre thème en cours d'installation correspond 
bien au "prefix" contenu dans le fichier config.json. Si ce n'est pas le cas, renommer le nom de votre répertoire avec 
celui du "prefix".

Si le nom de répertoire est déjà pris, vous pouvez soit supprimer le répertoire déjà existant et le remplacer par 
le nouveau, ou aller dans le fichier config.json de votre nouveau répertoire de thème et remplacer le "prefix" par un 
nom qui n'est pas déjà pris (en vaillant toujours à vous assurer que le nom du répertoire correspond au nouveau "prefix" 
renseigné).

Puis aller sur votre site web et rendez-vous sur la page "administration" puis sur l'onglet "apparences" et enfin 
sélectionner votre thème.

Pensez bien à vider le cache de votre navigateur après avoir sélectionné votre thème.

