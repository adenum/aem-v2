# Cahier des charges fonctionnel

> 04 03 2022

**Remarque** : Ce document ne traite ni l’aspect visuel ou l’ergonomie de la plateforme.

## Descriptif de la plateforme

La plateforme **Agora Ex Machina** est dédiée à la *Prise de décision collective* grâce à un processus inspiré de la *Démocratie Liquide* : les participants à un **Thème** de discussion, dans le cadre d'**Ateliers**, font des **Propositions** qui peuvent être **Discutées** au travers d'un *Forum*, puis **Votées** ; la *Démocratie Liquide*, dite également *Délégative*, propose qu'on puisse **Déléguer** son **Vote** à une autre personne.

La *Prise de décision* peut se faire grâce à trois types de votes :

* Le **Vote** à trois niveaux (*Pour*, *Contre*, *Abstention*)
* Le **Vote** à cinq niveaux (*Totalement d'accord*, *D'accord*, *Assez d'accord*, *Pas d'accord*, *Pas du tout d'accord*)
* Le **Vote** à points.

### Historique

En 2012, le programmeur **Cyril Bazin** (crlbazin@gmail.com) a développé une première version opérationnelle de cette plateforme. Sur les principes de la *Démocratie liquide* et de la plateforme allemande *Adhocracy*, ce logiciel privilégie quatre axes :

* Les **Votes** valident ou invalident des **Propositions** regroupés dans des **Ateliers**, eux-mêmes regroupés dans des **Thèmes** communs
* Des **Administrateurs** et **Administrateurs Restreints** organisent le fonctionnement général de la plateforme en organisant **Thèmes** et **Ateliers**. Des **Modérateurs** accompagnent les **Participants** dans leurs prises de décision.
* Toute personne inscrite peut participer au **Vote**, écrire des **Propositions** et participer aux **Discussions** commentant les **Propositions**
* Toute personne inscrite peut **Déléguer** son **Vote** à une autre personne dont il considère l’expertise.

En 2019, **Cyril Bazin** a adapté son développement en utilisant le *Framework* **Symfony** (version 4.4).

En 2021, deux développeurs en formation continue, **Valentin Trouillet** ([trouillet.valentin@gmail.com](mailto:trouillet.valentin@gmail.com)) et **Keyttlin Malicieux** ([keyttlin.m@hotmail.com](mailto:keyttlin.m@hotmail.com)), ont amplifié le développement de la plateforme

* passage à Symfony 5
* Construction d'un plateforme complètement opérationnelle.

En parallèle **François Mauviard** ([fmauviard@mobile-adenum.fr](mailto:fmauviard@mobile-adenum.fr)), au nom de l'Association **ADENÜM**, a mis en place le présent Cahier des Charges et s'occupe du suivi du projet jusqu'à son entière réalisation.

### Choix techniques

Cette plateforme légère est développée avec des technologies Web éprouvées et simples à installer (php/mySQL) et à mettre en place. À terme, elle doit devenir une sorte de **Wordpress** pouvant être facilement installée sur un serveur avec le moins de gestes techniques possible, et utilisable par une équipe d'**Admnistrateurs** ne s'intéressant pas aux techniques en informatique.

### Porteur du projet

Cette plateforme est voulue légère de sorte à faciliter son installation et son utilisation. Elle est développée dans le cadre d'un projet de l'association **ADENÜM**. Existant depuis 2013, cette association a pour objet de favoriser la culture et les pratiques numériques pour tous. Pour se faire, elle propose ses services auprès de structures institutionnelles, associatives ou privées. Elle apporte tous les outils qui permettront à nos concitoyens de devenir créatifs face aux enjeux numériques, en favorisant une approche *Opensource*.

Dans ce cadre, elle a considéré que jusqu'à présent, il n'existait pas jusqu'à présent d'une telle plateforme permettant la *Prise de décision collective*, et a donc décidé de prendre en main le développement d'**Agora Ex Machina** et de l'accompagner jusqu'à son entière réalisation.

## Descriptif des Fonctionnalités

### Catégories

Les **Catégories** permettent de regrouper les **Utilisateurs** au sein de *Cohortes* de **Participants** intéressées par les mêmes thématiques. 

> On peut regrouper au sein d'une même **Catégorie** les habitants d'une ville, les membres d'un association, un groupe de travail.

Les **Participants** inscrits à une **Catégorie** n'ont pas connaissance des **Thèmes**, **Ateliers**, **Discussions** et **Votes** qui ont lieu dans d'autres **Catégories**.

### Thèmes

Les **Thèmes** permettent de regrouper des **Ateliers** dans lesquels les **Participants** inscrits à la **Catégorie** qui les regroupent pourront participer aux **Propositions**, **Discussions** et **Votes** les concernant.

Les **Thèmes** permettent également de définir le type de **Vote** employé et la profondeur de **Délégation**. Enfin, un **Thème** peut être *Public* ou non ; dans ce cas, il pourra être affiché dans l'interface publique du site ; des personnes non loguées peuvent alors consulter l'ensemble des processus démocratiques associés à ces **Thèmes** sans pour autant y participer, à moins qu'ils s'y inscrivent.

> Si on prend l'exemple d'une **Catégorie** *Résidents de la ville de ...*, les **Thèmes** peuvent par exemple être : Pistes cyclables ; Attribution de budgets ; Organisation des fêtes de fin d'année ; Assemblée Générale d'une association ; ...

Il est possible de caractériser un **Thème** par un ou plusieurs **Mot(s)-clef(s)**.

### Ateliers

Les **Ateliers** définissent un cadre dans lequel les **Utilisateurs** pourront faire des **Propositions**, **Discuter** chacune d'entre elles et **Voter** en connaissance de cause.

Les **Ateliers** permettent de définir :

* le début et la fin de la période de **Discussion**
* le début et la fin de la période de **Vote**
* le quorum du **Vote**
* des **Mots-clefs** associés.

> Si on prend l''exemple d'un **Thème** *Pistes Cyclables*, un **Atelier** peut correspondre à : Signalisation au sol rue du Maréchal de Gaulle ; Panneaux de signalisation rue du Maréchal Joffre ; Informations de la mairie et communication ; ...

On peut associer à un **Atelier** un ou plusieurs **Documents** qui permettent d'alimenter la **Discussion** et le choix de chacun pour ses **Votes**.

### Propositions

Les **Propositions** sont faites par l'ensemble des **Utilisateurs** inscrits à la **Catégorie** correspondante. Ces **Propositions** peuvent être discutées au sein d'un *Forum* et **Votées**. Les périodes de **Discussions** et de **Votes** peuvent être synchrones ou séparées.

Dans le cadre d'un **Atelier** dédié à la *Signalisation au sol* dans un **Thème** dédié aux *Pistes cyclables*, une proposition peut être :

* ajouter des plots pour protéger les pistes cyclables signalées au sol à l'abord du carrefour X
* agrandir les icones au sol signifiant la circulation des vélos
* ...

Il peut s'avérer qu'une **Proposition** ne corresponde pas à la *Charte des Participants* : elle peut donc, si l'**Administrateur** en donne la possibilité, être signalée par un **Participant**.

### Discussions

Les **Discussions** se font au travers de chats. Une **Discussion** est associée à chaque **Proposition**. Toute personne (c'est-à-dire tout **Utilisateur** inscrit à la **Catégorie**) peut participer aux **Discussions**.

Là encore, un système de **Signalement** est prévu.

### Votes

Les **Votes** sont exprimés par tout **Participant** inscrit à la **Catégorie**.

Il existe trois types de **Votes** :

* Le **Vote** à trois niveaux (*Pour*, *Contre*, *Abstention*)
* Le **Vote** à cinq niveaux (*Totalement d'accord*, *D'accord*, *assez d'accord*, *Pas d'accord*, *Pas du tout d'accord*)
* Le **Vote** à points : dans ce cas, un nombre de points est attribué lors de la création du **Thème** à tout participant. Ce dernier distribue ses points aux propositions en fonction de ses choix et ses opinions.

#### Délégation

Les **Votes** à trois et à cinq niveaux peuvent être, si l'**Administrateur** en donne la possibilité, **Délégués** à un autre **Participant**. C'est le principe même de la *démocratie* dite *délégative* ou *liquide*.

La chaîne de **Délégation** peut également être configurée par l'**Administrateur** : c'est lui qui décide combien de **Délégations** en chaîne peuvent avoir lieu.

### Signalements

Un **Signalement** est fait par tout-e participant-e qui considère qu'une **Proposition** ou une expression dans une **Discussion** ne correspond pas aux standards de la communauté, c'est-à-dire à la *Charte des Participants*. Les **Signalements** sont ensuite retransmis aux personnes concernées, c'est-à-dire :

* Aux **Administrateur(s) restreint(s)** et **Modérateurs(s)** du **Thème**
* À ou aux **Administrateurs(s)** si le **Thème** n'a pas d'**Administrateur Restreint** ou de **Modérateur** associé.

Un **Signalement** est signifié à la ou aux personnes concernées :

* sur son interface (menu **Signalements**)
* par email si l'**Utilisateur** permet que la plateforme lui envoie des emails.

## Descriptif des Utilisateurs

Les **Utilisateurs** sont toutes les personnes dûment inscrites sur la plateforme. Il existe quatre *Rôles* :

* Les **Administrateurs** (**ADMIN**) peuvent gérer l'ensemble de la plateforme
* Les **Administrateurs restreints** (**ADMIN_RESTREINT**) peuvent gérer tous les **Thèmes**, **Ateliers**, **Discussions** de la ou des **Catégories** auxquels ils ou elles sont inscrit-e-s
* Les **Modérateurs** (**MODERATEUR**) peuvent modérer l'ensemble des discussions des **Ateliers** auxquelles ils ou elles sont inscrit-e-s. Par **Modération**, on entend :
  * pouvoir indiquer à un auteur d'une **Proposition** ou d'un message de la **Discussion** qu'il contrevient à la *Charte des Participants*
  * pouvoir **Supprimer** une proposition ou un message de la **Discussion** pour les mêmes raisons
  * prévenir un **ADMIN** ou un **ADMIN_RESTREINT** de **Signalements**
* Les **Participants** (**USER**) participent à la vie démocratique de la plateforme pour toute **Catégorie** à ou auxquelles ils ou elles sont inscrit-e-s
* Les **Observateurs** (**OBSERVER**) peuvent venir voir ce qui se déroule au sein d'une **Catégorie**. Un **Observateur** peut être **Participant** au sein d'une autre **Catégorie** que celle dans laquelle il est **Observateur**.

### Inscription d'un nouvel Utilisateur

Toute personne peut être inscrite sur la plateforme soit de son propre fait (auto-inscription), soit par un **ADMIN**.

#### Auto-inscription

Dans ce cas, l'inscription se fait en plusieurs étapes :

* la personne s'inscrit grâce à un *Formulaire dédié*
* un *Email* l'informe de son inscription et l'incite à cliquer sur un *lien de validation* ...
* ... qui renvoie vers une page affichant la *Charte du Participant* et un formulaire l'invitant à valider son adhésion à la charte ;
* suite à la validation de la charte, un formulaire lui propose de s'inscrire à une ou plusieurs **Catégories**.

L'**ADMIN** de la plateforme ou les **ADMIN_RESTREINTs** inscrits à la ou aux **Catégorie(s)** choisies par la personne qui s'est inscrite sont alors prévenus par **Signalement** (sur la plateforme ou par *Email*) qu'une nouvelle personne demande à être inscrite à la ou aux **Catégories**. Ce sont eux qui valident son inscription.

À validation, le nouvel **Utilisateur** est informé-e de cette validation par email.

Le texte de cet email peut être défini par défaut, ou rédigé depuis l'interface privée de l'administrateur.

#### Inscription par un Administrateur

Dans ce cas, c'est l'**ADMIN** qui est chargé de cette inscription. Il peut le faire :

* par une page dédiée pour une inscription unique
* par une fonctionnalité dédiée à l'inscription par groupes (via un fichier *csv* par exemple).

Champs renseignés

* Nom d'utilisateur
* Prénom
* Nom
* email
* Mot de passe.

### Oubli de mot de passe

Une fonctionnalité doit permettre à tout utilisateur de changer son mot de passe en cas d'oubli. Phases de la *Modificatioon du mot de passe* :

* il clique sur un lien *Mot de passe Oublié*
* un formulaire l'invite à saisir son *Email*
* il ou elle reçoit un email l'invitant à cliquer sur un lien ...
* ... qui l'invite à changer son mot de passe.

### Modification d'un Utilisateur

#### Par l'utilisateur lui-même

Tout utilisateur peut modifier, par l'intermédiaire d'un lien *Mon identité* dans le menu principal toute ou partie des informations correspondant à son profil :

* *Nom d'utilisateur*
* *Prénom*
* *Nom*
* *Email*
* *Mot de passe*
* *Mot de passe (vérification)*.

De plus, il lui est possible d'associer à son profil une image de son choix.

#### Par un ADMIN

Le formulaire de modification d'un **Utilisateur** dans l'espace **Administration** (pour **ADMIN** uniquement) permet de modifier les informations suivantes :

* Nom d'utilisateur
* email
* mot de passe.

Un **ADMIN** peut également :

* Attribuer un autre rôle que celui d'**USER** (à savoir **ADMIN**, **ADMIN_RESTREINT** ou **MODERATEUR**) à la personne
* Inscrire la personne à une ou plusieurs **Catégories**
* la bannir temporairement ou définitivement.

Remarque concernant les **ADMIN(s)**.

* Un **ADMIN** a accès à tous les éléments éditoriaux concernant l'entièreté de la plateforme, à savoir les **Catégories**, les **Thèmes**, les **Ateliers** et les **Mots-Clefs**. Il retrouve tous ces éléments dans la partie privée (**Administration**) et peut naviguer dans toutes les pages publiques.
* Par contre, il ne peut ni faire de **Propositions**, ni **Discuter**, ni **Voter** ou **Déléguer** son vote s'il n'est pas inscrit à la **Catégorie** englobante. De la même façon, il ne peut recevoir de **Signalements** concernant les inscriptions aux **Catégories**, les **Propositions** ou les **Discussions** que dans les mêmes conditions.

Un **ADMIN_RESTREINT** peut :

* attribuer un autre rôle que celui d'**USER** (**MODERATEUR**) à la personne
* la bannir temporairement ou définitivement.

## Descriptif des Pages - Partie privée

La partie privée, accessible uniquement aux **ADMINs** et **ADMIN_RESTREINTs**, est accessible via le menu principal à l'entrée *Administration*.

Pour chaque Page, lorsque cela est nécessaire, un message en haut de page indique ce qu'il est possible de faire.

### Tableau de bord (Page d'accueil de la partie **Administration**)

Cette partie est essentiellement informative. Elle renseigne l'**ADMIN** ou l'**ADMIN_RESTREINT** du nombre :

* d'*Utilisateurs* (à ne pas confondre avec le nombre de **Participants** : il s'agit du nombre total de personnes inscrites et non bannies sur la plateforme).
* de **Thèmes**
* d'**Ateliers**
* de **Catégories** (**ADMIN** uniquement)
* de **Mots-Clefs** utilisés.

À chaque encart est associé un *lien hypertexte* renvoyant à la fonctionnalité concernée, en doublon avec le menu haut.

* Un **ADMIN** a accès à la totalité des informations collectées.
* Un **ADMIN_RESTREINT** ne doit voir comptabilisés, pour ce qui concerne les **Thèmes** et les **Ateliers**, que ceux qui sont associés à la ou aux **Catégories** auxquels il est inscrit. De plus, pour ce qui concerne les **USERs**, il n'a accès qu'aux **USERs** étant inscrits à la ou aux même(s) **Catégories** que lui. Il n'a pas accès aux profils des **ADMIN**.

### Configuration (ADMIN uniquement)

Cette page regroupe et permet d'éditer l'ensemble des informations relatives au site :

* *Nom* : il s'agit de la structure qui héberge la plateforme (affiché dans le pied de page de toutes les pages du site)
* *Version* : il s'agit du copyright (affiché dans le pied de page de toutes les pages du site)
* *Titre* du site (indiqué dans les balises `meta` du `head` et en haut de toutes les pages
* *Url* (optionnel) : permet d'agir en réécriture d'urls, par exemple pour se débarrasser du suffixe `/public`
* *Logo* principal du site. Par *Logo* on entend l'image qui s'affiche pour tous les **Catégories**, **Thèmes** et **Ateliers** pour lesquels aucun *Logo* n'a été spécifié (voir *Règles d'affichage des Logos* plus loin)
* *Email* : il s'agit de celui qui identifie tous les envois de mails
* *Message de Connection* (optionnel) : il s'agit du message affiché lorsque quelqu'un se connecte sur la page *login*
* *Message d'enregistrement* (optionnel) : il s'agit du message affiché lorsque quelqu'un se connecte sur la page *register*
* *Image* (optionnel) : il s'agit de l'image d'arrière-plan pour les pages du site
* *Couleur* (optionnel) : c'est la couleur d'arrière-plan utilisée pour le site.

À l'installation de la Plateforme, seules les informations *Nom*, *Version*, *Titre*, *Email* sont renseignées.

### Emails

Cette page regroupe les différents messages envoyés par la plateforme automatiquement lors d'un signalement ou d'une inscription.

Noter qu'il doit y avoir systématiquement possibilité d'un message par défaut, inclus par exemple dans messages.lang.xlf.

À compléter.

### Utilisateurs

Cette page regroupe l'ensemble des **Utilisateurs**. À l'installation de la plateforme, il n'existe qu'un seul **Utilisateur ADMIN**.

### Page d'accueil de la Section Utilisateurs

Une liste (en mode tableau) recense l'ensemble des **Utilisateurs** inscrits (par ordre alphabétique). Au delà de 20 Utilisateurs, un système de pagination doit apparaître (onglets avec initiales des *Noms* des Utilisateurs s'il y en a. Pour chaque utilisateur (ligne du tableau) sont affichés :

* *Avatar*
* *Nom d'utilisateur*
* la ou les *Catégorie(s)* au(x)quelle(s) iel est inscrit-e
* *Email*
* une icône pour modifier l'*Utilisateur*
* une icône pour supprimer l'*Utilisateur*.

Deux boutons, en haut de l'encart des *Utilisateurs*, permettent :

* de créer un nouvel *Utilisateur*
* de créer plusieurs *Utilisateurs* (création groupée).

#### Créer un nouvel Utilisateur

Un formulaire avec les champs :

* *Nom d'utilisateur* (texte)
* *Prénom* (texte)
* *Nom* (texte)
* *Email* (texte)
* *Mot de passe* (texte)
* *Mot de passe* (pour vérification)
* *Rôle*. Liste déroulante. Le rôle par défaut est *Participant*. Noter que le rôle *Observateur* n'est pas attribué dans ce cadre, mais dans la Catégorie.
* Cases à cocher avec les différentes **Catégories**. Pas de valeur défaut.
* Deux boutons d'enregistrement :
  * *Enregistrer* ramène à l'écran principal des *Utilisateurs*
  * *Enregistrer et créer un nouvel Utilisateur* fait réapparaît le formulaire dont les champs sont réinitialisés.

Aucun champ n'est optionnel.

#### Création groupée

Un formulaire de téléchargement apparaît.

* Une information indique le format à utiliser
* Un filtre sur le champ de téléchargement empêche tout format qui ne serait pas accepté
* Un bouton *Prévisualiser* permet d'afficher la page suivante permettant de vérifier que le fichier a été correctement formaté (affichage en ligne des importations). Sur cette seconde page, à la suite de l'affichage de prévisualisation, deux boutons :
  * *Annuler* : l'importation est annulée.
  * *Confirmer* : l'importation est validée.

Sous l'explication, au dessus du formulaire, un bouton permet de revenir à la page d'administration des **Utilisateurs**.

Tous les *Rôles* sont alors définis par défaut comme **USER**.

#### Modifier un Utilisateur

Un formulaire avec les champs préremplis :

* *Nom d'utilisateur*
* *Email*
* *mot de passe*
* *Mot de passe* (pour vérification du mot de passe précédent)
* Cases à cocher avec les différentes *Catégories*. Pas de valeur défaut.

Seul le choix des **Catégories** est optionnel.

#### Supprimer un Utilisateur

À la demande de suppression, une modale apparaît demandant la confirmation.

### Catégories

#### Page d'accueil de la Section Catégories

Cette page recense les différentes **Catégories** auxquelles peuvent être inscrits les **Thèmes**. La page recense l'ensemble des **Catégories** créées pour le site (par ordre d'*id*). À l'installation de la plateforme, il n'existe qu'une seule **Catégorie** *Défaut*.

Liste (en mode tableau) :

* *Logo* de la **Catégorie**
* *Titre* de la **Catégorie**
* Les 60 premiers caractères de la *Description*
* Un bouton *Modifier*
* Un bouton *Supprimer*

Sous le message d'aide descriptif et au dessus de la liste, un bouton *Ajouter* permet d'ajouter une **Catégorie**.

#### Ajouter une Catégorie

Un formulaire avec les champs :

* *Titre* (texte)
* *Description* (Optionnel Textarea CKEditor)
* Une liste de toutes les personnes inscrites sur la plateforme avec des cases à cocher (Une catégorie peut n'avoir aucune personne inscrite)
* *Logo* (format jpg/gif/png/svg)
* *Enregistrer* - Bouton.

À *Validation*, la **Catégorie** est créée.

Au dessus du formulaire, un bouton *Liste des Catégories* permet de revenir en arrière (sans vérification des champs).

#### Éditer une Catégorie

Deux formulaires :

* À gauche, un formulaire de soumission du *Logo* :
  * Si un *Logo* a déjà été soumis, on peut supprimer le *Logo*. On peut également en soumettre un nouveau.
  * Si un *Logo* n'a pas encore été soumis, c'est le logo général d'**AEM** qui est affiché.
  * Bouton *Valider*. À *Validation*, on reste sur la page.
* À droite, un formulaire avec les champs :
  * *Titre* (texte)
  * *Description* (Optionnel Textarea CKEditor)
  * En deux colonnes, deux *Listes* de toutes les personnes inscrites sur la plateforme - cases à cocher (Une catégorie peut n'avoir aucune personne inscrite), avec à gauche les **participants**, à droite les **Observateurs**. Au dessus de chaque colonne, un texte explicatif décrit la différence entre les **Participants** et les **Observateurs**.
  * Un bouton *Valider*. À *Validation*, on reste sur la page.

Sous le message d'explication, au dessus des deux formulaires, deux boutons :

* *Liste des Catégories* permet de revenir à la page d'accueil de l'administration des **Catégories**. Si une modification a été effectuée dans un des champs, une modale demande à valider le choix.
* *Supprimer* permet de supprimer la **Catégorie**. Une modale demande à valider le choix. À validation de la modale, on revient à la page d'accueil de l'administration des **Catégories**.

#### Supprimer une Catégorie

Permet de supprimer la **Catégorie**. Une modale demande à valider le choix. À validation de la modale, on reste sur la page d'accueil de l'Administration des **Catégories**.

### Thèmes

#### Page d'accueil de la Section Thèmes

Cette page recense les différents **Thèmes** (par ordre d'*id*).

* pour un **ADMIN**, sans restriction
* pour un **ADMIN_RESTREINT**, seuls les **Thèmes** associés à la ou aux **Catégorie(s)** au(x)quel(s) il est inscrit sont affichés.

Liste (en mode tableau) :

* *Logo* du **Thème**
* *Catégorie* à laquelle il appartient
* *Titre* du **Thème**
* Les 60 premiers caractères de la *Description*
* Un bouton *Modifier*
* Un bouton *Supprimer*.

Sous le message d'aide descriptif et au dessus de la liste, un bouton *Ajouter* permet d'ajouter un **Thème**.

#### Ajouter un Thème

Un formulaire avec les champs :

* *Catégorie* (liste déroulante - valeur *Défaut* par défaut)
* *Titre* (texte)
* *Description* (Optionnel Textarea CKEditor)
* *Logo* (format jpg/gif/png/svg)
* *Public* (Case à cocher O/N)
* *Mots-clefs* (séparés par une virgule) ; un dispositif *ajax* permet l'écriture prédictive
* *Type de vote* (liste déroulante ou bouton radio au choix - *Vote à trois niveaux*, *Vote à cinq niveaux*, *Vote à points*). Par défaut, *Vote à trois niveaux*
  * Si ce sont les *Vote à trois niveaux* ou *Vote à cinq niveaux* qui sont sélectionnés, une case à cocher *Permettre la Délégation* est affichée - Par défaut, activée (*ajax*)
    * Si la Délégation est permise, un champ supplémentaire demande quelle doit être la profondeur de la **Délégation**. Pas de valeur par défaut ; Pas de valeur = 0 ; 0 = Délégation illimitée (*ajax*).
  * Si c'est le *Vote à points* qui est sélectionné, Un champ \* Nombre de points\* est affiché - Valeur 20 par défaut ; minimum 10 maximum 40 (*ajax*).
  * *Permettre le signalement* : case à cocher O/N. Non par défaut.
* Un bouton *Enregistrer*.

À validation, le **Thème** est créé.

Au dessus du formulaire, un bouton *Liste des Thèmes* permet de revenir en arrière (sans vérification des champs).

#### Éditer un Thème

Deux formulaires :

* À gauche, un formulaire de soumission du *Logo* :
  * Si un *Logo* a déjà été soumis, on peut supprimer le *Logo*. On peut également en soumettre un nouveau.
  * Si un *Logo* n'a pas encore été soumis voir le chapitre *Règle d'affichage des Logos*.
  * Bouton *Valider*. À *Validation*, on reste sur la page.
* À droite, un formulaire reprend l'ensemble des champs du Formulaire *Ajouter un Thème* préremplis, à l'exception du champ *Logo*. À *Validation*, on reste sur la page.

Sous le message d'explication, au dessus des deux formulaires, deux boutons :

* *Liste des Thèmes* permet de revenir à la page d'accueil de l'administration des **Thèmes**. Si une modification a été effectuée, une modale demande à valider le choix.
* *Supprimer* permet de supprimer le **Thème**. Une modale demande à valider le choix. À validation de la modale, on revient à la page d'accueil de l'administration des **Thèmes**

#### Supprimer un Thème

Permet de supprimer le **Thème**. Une modale demande à valider le choix. À validation de la modale, on reste sur la page d'accueil de l'administration des **Thèmes**.

### Ateliers

#### Page d'accueil de la Section Ateliers

Cette page recense les différents **Ateliers** (par ordre d'*id*) :

* pour un **ADMIN**, sans restriction
* pour un **ADMIN_RESTREINT**, seuls les **Ateliers** lié à la ou aux **Catégorie(s)** au(x)quel(s) il est inscrit sont affichés.

Liste (en mode tableau) :

* \*Logo \*de l'**Atelier**
* Le **Thème** auquel il appartient
* *Titre* de l'**Atelier**
* Les 60 premiers caractères de la *Description*
* Un bouton *Modifier*
* Un bouton *Supprimer*.

Sous le message d'aide descriptif et au dessus de la liste, un bouton *Ajouter* permet d'ajouter un **Atelier**.

#### Ajouter un Thème

Un formulaire avec les champs :

* *Thème* (liste déroulante - le **Thème** par défaut est le premier dans la liste des *Ids* pour un **ADMIN**, le premier auquel un **ADMIN_RESTREINT** est inscrit sinon)
* *Titre* (texte)
* *Description* (Optionnel Textarea CKEditor)
* *Logo* (format jpg/gif/png/svg)
* *Date de début des discussions*
* *Date de fin des discussions*
* *Date de début des Votes*
* *Date de fin des Votes*
* *Quorum*
* *Mots-clefs* (séparés par une virgule) ; un dispositif *ajax* permet l'écriture prédictive (*ajax*)
* Bouton *Enregistrer*.

À validation, l'**Atelier** est créé.

Au dessus du formulaire, un bouton *Liste des Ateliers* permet de revenir en arrière (sans vérification des champs).

#### Éditer un Atelier

Deux formulaires :

* À gauche, un formulaire de soumission du *Logo* :
  * Si un *Logo* a déjà été soumis, on peut supprimer le *Logo*. On peut également en soumettre un nouveau.
  * Si un *Logo* n'a pas encore été soumis voir le chapitre Règle d'affichage des *Logos*.
  * Bouton *Valider*. À Validation, on reste sur la page.
* À droite, un formulaire reprend l'ensemble des champs du Formulaire *Ajouter un Atelier* préremplis, à l'exception du champ *Logo*. À *Validation*, on reste sur la page.

Sous le message d'explication, au dessus des deux formulaires, deux boutons :

* *Liste des Ateliers* permet de revenir à la page d'accueil de l'administration des **Ateliers**. Si une modification a été effectuée dans le formulaire, une modale demande à valider le choix.
* *Supprimer* permet de supprimer l'**Atelier**. Une modale demande à valider le choix. À validation de la modale, on revient à la page d'accueil de l'administration des **Ateliers**
* *Ajouter un document*
* *Gestion des Documents* (Voir chapitre suivant).

#### Supprimer un Atelier

Permet de supprimer l'**Atelier**. Une modale demande à valider le choix. À validation de la modale, on reste sur la page d'accueil de l'administration des **Ateliers**.

### Documents

On peut associer à un **Atelier** un ou plusieurs **Documents** qui permettent d'alimenter la **Discussion** et le choix de chacun. La gestion des **Documents** se fait depuis la page de gestion d'un **Atelier**.

#### Ajouter un document

Un formulaire avec :

* *Champ de téléchargement*
* *Nom* à donner au document
* *Enregistrer*. À Validation, retour vers l'atelier auquel le **Document** est attaché.

Sous l'explication proposée à l'**ADMIN** ou l'**ADMIN_RESTREINT** et au dessus du formulaire, deux boutons :

* *Retour à l'Atelier*
* *Gérer les Documents*

#### Gérer les documents

Une liste reprenant l'ensemble des **Documents** associés à l'**Atelier** (Tableau).

* *Icone* pdf
* *Nom* du **Document**
* *Nom* du fichier
* *Supprimer*

À suppression du fichier, une modale apparaît demandant la confirmation. À confirmation, on reste sur la page raffraichie.

Sous l'explication, et au dessus du Tableau, deux boutons :

* *Retour à l'Atelier*
* *Ajouter un document*

### Mots-clefs

Cette page recense les différents **Mots-clefs** (par ordre d'*id*) :

* N° *Id* du Mot-clef
* *Intitulé* du mot-clef
* Liste des *Titres* des **Thèmes** au(x)quel(s) le mot clef est associé (séparés par des virgules)
* Liste des *Titres* des **Ateliers** au(x)quel(s) le mot clef est associé (séparés par des virgules)
* *Icone Supprimer*.

La liste des **Thèmes** et des **Ateliers** pour chacun des **Mots-clefs** est restreinte, pour les **ADMIN_RESTREINTs**, à la ou aux **Catégorie(s)** au(x)quelle(s) il est inscrit.

Ni les **Mots-clefs**, ni les **Thèmes** ou les **Ateliers** n'ont de lien hypertexte.

### Votes

Cette page simple définit pour chaque **ADMIN** ou **ADMIN_RESTREINT** inscrit à une **Catégorie** d'accéder aux résultats des **Votes** des **Ateliers** associés à cette ou ces **Catégorie(s)**.

Elle ne permet pas de savoir ce qu'a voté un **Participant**. En revanche, elle permet de savoir qui a voté (en son nom propre ; en cas de **Délégation**, le vote compté est celui du délégué correspondant.

#### Présentation

Classés par **Catégories** puis **Thèmes**, les **Ateliers** sont présentés par ordre d'*id* en mode Tableau :

* Le nom de l'**Atelier**
* État de l'**Atelier** (*Non débuté*, *Discussion en cours*, *Discussion et Vote en cours*, *Vote en cours*, *Consultation terminée*).
* Nombre de votes exprimé
* Si le vote a débuté, ou si la consultation est finie, *Prénom Nom* des personnes ayant voté (ou ayant voté par délégation), totalement dans le cas du vote à points.

Lorsque les consultation de l'ensemble des **Ateliers** d'un **Thème** sont terminés, un fichier csv (en dernière colonne du tableau) reprenant l'ensemble des informations pour la totalité des \**Ateliers* peut être téléchargé.

## Descriptif des Pages - Partie publique

### Toutes les pages

* Le *Titre* de la page est celui défini dans le *Titre* défini dans la page `Administration/Configuration`. Le lien renvoie à la page d'accueil.
* Une *Icône* à gauche est un lien qui ouvre le menu principal (latéral)
* Au centre, un premier encart apporte des messages explicatifs sur la plateforme :
  * Un titre de niveau 2
  * Plusieurs paragraphes.

Les en-têtes des balises `meta` dans le `<head>` font référence au *Titre*, à la *Description*, au *Logo*, à l'auteur (*Nom*). Un jeu d'icônes de base est fournée avec la plateforme.

#### Menu principal

Le menu principal permet aux **Utilisateurs** de la plateforme d'accéder directement à des fonctionnalités spécifiques :

* Retour à l'*Accueil*
* *Mon Identité* (voir plus haut)
* *Mes Délégations*
* *Notifications*
* *Administration* (pour **ADMIN** et **ADMIN_RESTREINT** uniquement)
* *Se déconnecter*.

En en-tête du menu principal, sont affichés l'*avatar* (soit les deux initiales *Prénom Nom*, soit l'image uploadée par l'utilisateur) et le *Nom d'utilisateur* de la personne loguée.

Le menu latéral n'est affiché qu'après avoir cliqué sur le bouton Menu (Remarque pour l'UX).

#### Pied de page

* À gauche, le message défini dans le champ *Version* du formulaire `Administration/Configuration`
* À droite, le message défini dans le champ *Nom* du formulaire `Administration/Configuration`.

### Page d'accueil (non logué)

La page d'accueil recense l'ensemble des **Thèmes** définis en mode *Public*. Un lien renvoie à la page de **Thème** qui lui est consacrée.

* S'il y a au moins un **Thème** défini (un **ADMIN** a alors déjà commencé au moins à configurer la plateforme), deux boutons *Je veux participer* et *J'ai déjà un compte* sont affichés sous la liste des **Thèmes** *Publics*.
* S'il n'y a pas encore de **Thème** défini, seul le bouton *J'ai déjà un compte* est affiché.

### Page d'accueil (logué)

Les messages d'explication peuvent être différents de ceux de la page d'accueil non loguée et les messages `jumbotron` peutvent être différenciés et choisis par le développeur du `Template`.

La page d'accueil recense l'ensemble des **Thèmes** auquel l'**Utilisateur** est inscrit sous la forme d'encarts.

#### Règles d'affichage des encarts de Thèmes

Chaque encart est constitué :

* du *Logo* du **Thème**
* de son *Titre*
* de la **Catégorie** à laquelle il appartient
* de sa *Description*
* En pied d'encart sont affichés les nombres d'**Ateliers**, de **Propositions** et de *Threads* de **Discussion**.

Un lien associé au *Logo* et au *Titre* permet d'accéder à la page du **Thème** correspondant.

### Page de Thème

Une page de **Thème** recense les informations relatives au **Thème** et la liste des **Ateliers** rattaché à ce dernier.

En haut de page, sous le *Titre* de la plateforme (`<h1>`), on trouve le *Titre* du **Thème** (`<h2>`). Puis un encart avec :

* à gauche le logo du **Thème**
* à droite :
  * Le *Descriptif* du **Thème**
  * La **Catégorie** à laquelle le **Thème** appartient (sans lien hypertexte)
  * Les **Mots-clefs** du **Thème** (avec lien hypertexte, sauf si la personne qui consulte le Thème n'est pas loguée).

Sous le descriptif général du **Thème** on retrouve les descriptifs en encart des différents **Ateliers**.

Entre les deux, une série de boutons :

* À gauche, un bouton *Ajouter* (**ADMIN** et **ADMIN_RESTREINT** uniquement)
* À droite (de gauche à droite) :
  * Un bouton *Je délègue mon vote* si la délégation est acceeptée dans le **Thème**
  * Deux boutons permettant de faire varier l'affichage (mode *List* et *Grid*) de présentation des différents **Ateliers**.

#### Règles d'affichage des encarts d'Ateliers

Les **Ateliers** peuvent être affichés sous forme d'encart (*Grid*) ou de liste (*List*).

##### Grid

Les encarts sont affichés au format type Card Bootstrap. Y sont représentés :

* Le *Logo* de l'**Atelier** ( Voir Règles d'affichages des Logos)
* Son *Titre*
* Sa *Description* (150 caractères)
* Le nombre de **Propositions**
* Le nombre de *Threads* de **Discussion**
* Des codes couleur indiquent :
  * Si l'**Atelier** est en période d'activité
  * Si les **Discussions** sont ouvertes
  * si les **Votes** sont ouverts.

##### List

Les mêmes indications sont présentées sous forme de Tableau (en colonnes).

#### Ajouter un Atelier

Cette page est quasiment identique à celle accessible depuis l'**Administration** privée du site. Le titre *Ajouter un Atelier* (`<h3>`) et le lien *Retour vers la liste des ateliers du Thème ...*. elle n'est accessible qu'aux **ADMINs** et **ADMIN_RESTREINTs**

#### Je délègue mon vote

En cliquant sur ce bouton, une modale apparaît, proposant un formulaire de soumission : un menu déroulant avec la liste des personnes inscrite à la même **Catégorie** associée au **Thème** en cours. Un bouton *Enregistrer* ferme la modale et valide le choix. Possibilité de fermer la modale sans confirmation.

### Page d'Atelier

Une page d'**Atelier** recense les informations relatives à ce dernier et la liste des **Propositions** faites. Elle présente également les **Documents** attachés à l'atelier.

En haut de page, sous le *Titre* de la plateforme(`<h1>`), on trouve le *Titre* de l'**Atelier** (`<h2>`). Puis un encart avec :

* à gauche le logo de l'**Atelier**
* à droite :
  * Le *Descriptif* de l'**Atelier**
  * La **Catégorie** et le **Thème** auquel est rattaché l'**Atelier**
  * Les **Mots-clefs** de l'**Atelier** (avec lien hypertexte)
  * Les **Documents** rattachés à l'**Atelier**.

Sous le descriptif général de l'**Atelier** on trouve *Titres* et *Descriptifs* en encart des différentes **Propositions**.

Entre les deux, une série de boutons :

* À gauche, un bouton *Retour à la liste des Ateliers*
* À droite, un bouton *Faire une Proposition*.

#### Faire un Proposition

Au dessous de l'encart de la présentation de l'atelier, un formmulaire s'ouvre. Une **Proposition** est constituée :

* d'un *Titre* (texte)
* d'une *Description* (textarea CKEditor)
* d'un bouton *Proposer* pour valider.

À validation, Retour vers la page précédente, raffraichie.

Entre la présentation de l'**Atelier** et le Formulaire, un bouton *Retour vers l'Atelier*.

#### Affichage des encarts de Propositions

Sous un titre **Propositions** (`<h3>`), ces dernières peuvent être affichées :

* sous format *Grid*
* sous format *List*.

##### Grid

Encarts sur deux colonnes type format *Card* :

* Le *Titre* est cliquable
* *Descriptif*.
* Nombre de Votes effectué (arrère-plan grisé si le vote n'est pas encore commencé ou est terminé)
* Nombre de **Threads** de **Discussions** (grisé si la période de discussion n'a pas encore commencée ou est terminée). À la fin de la consultation, une information indique si le *Quorum* est atteint\*.
* Si la personne loguée est l'auteur de la proposition, deux boutons : *Modifier* et *Supprimer*
* Enfin, un bouton *Signaler* (bouton ON/OFF) permet au **Participant** de signaler la **Proposition**.

##### List

Affichage sous forme de Tableau :

* *Titre*
* *Descriptif*
* Nombre de **Votes** effectué (cellule grisée si le vote n'est pas encore commencé ou est terminé)
* Nombre de **Threads** de **Discussions** (cellule grisée si la période de discussion n'a pas encore commencée ou est terminée)
* Bouton *Modifier* (si la personne loguée est l'auteur de la proposition)
* Bouton *Supprimer* (si la personne loguée est l'auteur de la proposition)
* Bouton *Signaler*.

En cliquant sur le *Titre* de la **Proposition**, une fenêtre modale s'ouvre.

#### Fenêtre modale de Discussion et de Vote

La fenêtre qui apparaît présente le *Titre*, le *Descriptif* et l'*Auteur* (Nom d'utilisateur et avatar ou initiales des prénom et nom) de la **Proposition**.

Sous cette première partie apparaîssent :

* si l'interaction est en période de **Discussion**, *ajouter un sujet de discussion*
* si l'interaction est en période de **Vote** et que la personne n'a **Délégué** son vote à personne, un système de **Vote** :
  * Si le **Thème** est réglé sur un système de vote à trois niveaux, 3 boutons, *Pour*, *Contre*, *Abstention*
  * Si le **Thème** est réglé sur un système de vote à cinq niveaux, 5 boutons, *Tout à fait d'accord*, *D'accord*, *Plus ou moins d'accord*, *Pas d'accord*,*Pas du tout d'accord*
  * Si le **Thème** est réglé sur un système de vote à points, boutons radio (du nombre de points pas encore utilisé) de 1 à n.

**Remarques** :

* les **ADMINs** ou **ADMIN_RESTREINTs** peuvent décider de séparer ou non la période de **Discussions** et celle de **Votes**.
* Si la personne a **Délégué** son vote, cette \*\*délégation lui est rappelée (*Vous avez délégué votre vote à Nom d'Utilisateur*).

En dessous, si la période de **Vote** est terminée, les scores partiel (des différents votes) et total (le nombre total de **Votes** ou de points) est affiché. Est également indiqué si le quorum est atteint.

En dessous le bloc des boutons de **Discussions** ou/et de **Votes**, la discussion apparaît. Il s'agit d'un système de forum à profondeur de threads de 1. Les Threads et leurs réponses sont affichées en vis à vis.

Sous chaque *Thread* de **Discussion** apparaît un bouton *Signaler* (si l'**ADMIN** ou l'**ADMIN_RESTREINT** en charge de ce **Thème** a activé cette fonctionnalité).

### Page Mots-clefs

Cette page recense l'ensembles des **Ateliers** caractérisés par un **Mot-Clef** présentés sous formes d'encart ou de liste (voir plus haut formats *List* et *Grid*). Les ateliers sont accessibles :

* aux **ADMINs** en totalité
* pour les autres rôles, seules les **Ateliers** liés à la ou aux **Catégorie(s)** à la ou auxquelle(s) l'**Utilisateur** est inscrit.

### Page Délégations

Cette page est informative et recense l'ensemble des **Délégations** qu'un **Participant** (**USER**) à fait à d'autres personnes, ainsi que celles qui ont été faites par d'autres à soi-même.

On ne peut récuser la **Délégation** faite par une autre personne. Par contre, on peut récuser la **Délégation** faite à une autre personne.

Ce sont deux Tableaux. Le premier présente les *Délégations données* à d'autres **Participants** , reprenant :

* Le *Titre* du **Thème**
* La liste des **Ateliers** concernés (*Titres*, séparés par une virgule)
* Le **Particpant** à qui est donnée la **Délégation**
* Une \*icone \* qui permet de *Retirer* la **Délégation**.

Le premier présente les *Délégations reçues*, reprenant :

* Le *Titre* du **Thème**
* La liste des **Ateliers** concernés (*Titres*, séparés par une virgule)
* Le **Participant** qui a donné la **Délégation**.

Il n'y a pas de bouton *Retirer* dans ce second Tableau.

### Page Notifications

Cette page est informative. Elle recense l'ensemble des **Notifications**, à savoir :

* L'acceptation ou le refus d'inscription à une **Catégorie**
* La réponse à un *Thread* de **Discussion** qui a été engagée par le **Participant**
* Les nouvelles **Discussions** engagées par d'autres **Participants**
* Les débuts et fin des **Votes** dans les **Ateliers**.

À ces **Signalements** s'ajoutent :

* celles faites aux **Modérateurs** :
  * **Signalement** fait par un **Participant** d'une **Proposition** ou d'une **Discussion** contrevenant éventuellement à la *Charte des Participants*
* celles faites aux **ADMIN_RESTREINTs** :
  * Demande d'inscription à une **Catégorie** dont l'**ADMIN_RESTREINT** à la charge
  * **Signalement** fait par un **Participant** d'une **Proposition** ou d'une **Discussion** contrevenant éventuellement à la *Charte des Participants*
  * **Signalement** fait par un **Moderateur** d'une demande de **Ban** temporaire ou définitif
* celles faites aux **ADMINs** reprennent celles faites aux **ADMIN_RESTREINTS**, uniquement celles auxquelles l'**ADMIN** est inscrit.

Chaque type de **Notification** est séparée par des sous-titres. Ces derniers apparaissent selon les droits accordés à l'**Utilisateur**.

**Remarque concernant l'UX**. La présentation peut être faite en encarts placés les uns sous les autres, ou en pages différenciées avec onglets en haut (modèle page ADMIN, mais avec code couleur Public).

#### Acceptations/refus d'inscription

Droits : **ADMINs** (toutes les demandes), **ADMIN RESTREINTs** (demandes associées à la ou aux **Catégorie(s)** auxquelles l'**ADMIN RESTREINT** est inscrit)

Sous-titre d'encart : *Inscription aux Catégories*. Affichage sous forme de Tableau / liste. Classement par id de `request` inverse et `request` traités (en premier) ou non.

* Icône indiquant si la demande a été traitée ou non. La ligne apparaît en gras si elle n'a pas été traitée.
* La date et l'heure auxquelles la notification a eu lieu
* Le *Nom d'utilisateur* concerné
* son *Email* (avec lien mailto: ou vers un formulaire d'envoi de mail / faire le descriptif de cette page)
* Si la demande n'a pas encore été traitée :
  * Bouton accepter
  * Bouton Refuser
* Si la demande a été traitée, rien.

#### Réponses dans les Discussions

Il s'agit de l'ensemble des réponses faites à des *Threads* que le **Participant** a saisis. Il s'agit donc, puisque le forum ne s'engage que sur deux niveaux (Une discussion engagée par un **Participant** et l'ensemble des réponses faites à cet premier *Thread*), des dates des derniers *Threads* engagés sur une **Discussion** auquel l'**Utilisateur** a participé.

Droits : Toutes les **Catégories** auxquelles l'**Utilisateur** est inscrit.

**Remarque** : un **ADMIN** peut très bien n'être inscrit à aucune **Catégorie**, étant donné qu'il peut Administrer la plateforme sans participer à aucune Consultation.

Sous-titre d'encart : *Discussions qui pourraient vous intéresser.* Affichage sous forme de Tableaux/liste (un par **atelier**. Classement par **Ateliers**, avec les *Threads* de **Discussions** du plus récent au plus ancien).

* Caption du tableau : *Titre* de l'**Atelier**
* *Titre* de la proposition
* *Titre* du *Thread* Principal (avec lien vers la page de l'**Atelier** et la discussion)
* Bouton/icône *Voir cette discussion* avec lien idem.

Une fois le lien utilisé (ou si la dernière participation à une discussion est celle faite par le **Participant**), la ligne disparaît des **Signalements**.

#### Votes

Cette page recense :

* les **Ateliers** pour lesquels le vote est en cours
* ceux pour lequel le vote est fini.

## Règles d'affichage

### Règle d'affichage des *Logos* (pour les parties publique et privée)

#### Pour le Site

* Si le *Logo* du site est déterminé, il est affiché
* Si le *Logo* du site n'est pas déterminé, c'est le *Logo* général de la plateforme qui est affiché.

#### Pour une Catégorie

* Si le *Logo* de la **Catégorie** est déterminé, il est affiché
* Si le *Logo* de la **Catégorie** n'est pas déterminé, c'est celui déterminé lorsque le site a été configuré qui est affiché
* Si ce n'a pas été fait, c'est celui général de la plateforme qui est affiché.

#### Pour un Thème

* Si le *Logo* du **Thème** est déterminé, il est affiché
* Si le *Logo* du **Thème** n'est pas déterminé, c'est celui de la **Catégorie** auquel il est rattaché qui est affiché
* Si le *Logo* de la **Catégorie** n'est pas déterminé, c'est celui déterminé lorsque le site a été configuré qui est affiché
* Si ce n'a pas été fait, c'est le *Logo* général de la plateforme qui est affiché.

#### Pour un Atelier

* Si le *Logo* de l'**Atelier** est déterminé, il est affiché
* Si le *Logo* de l'**Atelier** n'est pas déterminé, c'est celui du **Thème** qui est affiché
* Si le *Logo* du **Thème** n'est pas déterminé, c'est celui de la **Catégorie** auquel il est rattaché qui est affiché
* Si le *Logo* de la **Catégorie** n'est pas déterminé, c'est celui déterminé lorsque le site a été configuré qui est affiché
* Si ce n'a pas été fait, c'est le *Logo* général de la plateforme qui est affiché.

Les *Logos* peuvent être au formats suivants : jpg, gif, png, svg.

### Règles d'affichage des messages explicatifs

Les messages explicatifs sont déterminés dans le fichier `translations/messages.langue.xlf` avec un préfixe `jumbotron`. Ils sont exprimés au format `markdown`. Lors de leur affichage, un `filtre` dédié permet leur affichage au format `html`.

## Expérience Utilisateur

### Expérience générale

La simplicité, tant du point de vue du design que de l'ergonomie, doit prédominer. Cette simplcité doit se retrouver :

* dans l'utilisation d'icones explicites et visibles
* dans la localisation aisée des différentes fonctions
* dans la compréhension de ce qu'on peut faire.

Cette simplicité doit régir toutes les fonctions du site, tant pour le **Participant** à une **Catégorie** que pour tout autre rôle : **ADMIN**, **ADMIN_RESTREINT**, **MODERATEUR**.

#### Messages

Les messages doivent être explicites et clairs. Ils doivent communiquer l'essentiel en un minimum de mots. Ils doivent s'adresser à la personne en son nom personne : "Je m'identifie", "J'ai déjà un compte", "cet Atelier m'est accessible", sauf lorsque la personne n'est pas loguée.

Dans la partie **Administration**, les mesages doivent refléter un sérieux plus prégnant.

### Feuilles de style

L'interface publique et l'interface privées doivent être différenciées d'un point de vue aspect visuel : on doit savoir, à tout moment, et notamment pour les **ADMINs** et **ADMIN_RESTREINTs**, si on est dans la partie publique ou la partie **Administration**.

Du point de vue du développement, cela se traduit par deux feuilles de style différenciées, une pour la partie publique, une pour l'**Administration**.

#### Accessibilité

Un bouton ou lien doit permettre l'accessibilité de l'interface aux personnes mal voyantes. Seules les couleurs et éventuellement les tailles de caractères doivent être concernées. Du point de vue des fichiers, cela signifie que, tant pour la partie publique que privée, une feuille de style rendant accessible l'interface doit surcharger ce qui concerne les couleurs de la feuille de style.

### Différenciation selon le périphérique

#### Ordinateur et tablette

Toutes les fonctionnalités doivent être présentes, tant pour les \***ADMINs**, les **ADMIN_RESTREINTs**, les **MODERATEURs** que les **USERs**. Les pages doivent être adaptatives en fonction de la taille de l'écran.

À l'heure actuelle, c'est le *Framework* **Bootstrap** qui a été choisi. Il est très fortement conseillé de n'utiliser que celui-ci : notamment, les *Formulaires* développés sous **Symfony** l'ont été avec le *Bundle* **Bootstrap**. Dans l'idée d'une cohérence technique, seul ce *Framework* visant à permettre l'adaptabilité des pages aux périphériques de visionnage doit être utilisé.

Les pages doivent être affichées au format css *container* pour ce qui concerne le contenu. Les informations complémentaires (Menu Général, modales pour la **Discussion** et les **Votes**) peuvent être inclus dans un div *container-fluid* englobant.

#### Smartphone

Dans ce cas, seule la partie publique est accessible. Les gestes relatifs à l'**Administration** ne peuvent dont être réalisés que sur ordinateur ou tablette. Les fonctionnalités apparaissent en plein écran.

Le titre de la plateforme n'est plus visible, et est remplacé par l'icone de la plateforme.