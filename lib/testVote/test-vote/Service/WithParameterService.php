<?php

namespace TestVote\TestVoteBundle\Service;

class WithParameterService
{
    private string $str;

    public function __construct(string $str){
        $this->str = $str;
    }

    public function sayHello():string
    {
        return $this->str;
    }
}