<?php

namespace TestVote\TestVoteBundle\DependencyInjection;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class Configuration implements ConfigurationInterface
{

    function getConfigTreeBuilder()
    {
        $builder = new TreeBuilder('test_vote');

        $rootNode = $builder->getRootNode();
        $rootNode->children()
            ->scalarNode('vote1')
            ->isRequired()
            ->end()
            ->scalarNode('vote2')
            ->isRequired()
            ->end()
            ->scalarNode('vote3')
            ->isRequired()
            ->end()

            ->end();

        return $builder;
    }
}