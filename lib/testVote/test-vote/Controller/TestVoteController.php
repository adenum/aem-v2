<?php

namespace TestVote\TestVoteBundle\Controller;

use App\Entity\Proposal;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use TestVote\TestVoteBundle\Entity\VoteTest;


/**
 * Class VoteController Cette classe s'occupe de l'ajout et la suppression des votes
 * @package App\Controller
 */
class TestVoteController extends AbstractController
{
    /**
     * @param $vote
     * @param string $userVote
     * @param Proposal $proposal
     * @param User|null $user
     * @param ObjectManager $entityManager
     * Fonction qui traite le nouveau type de vote
     */
    public function voteTestVote($vote, string $userVote, Proposal $proposal, ?user $user, ObjectManager $entityManager): void
    {
        //Vote à 3 niveaux
        $vote->setVotedTest1(($userVote == "votedTest1") ? 1 : 0);
        $vote->setVotedTest2(($userVote == "votedTest2") ? 1 : 0);
        $vote->setVotedTest3(($userVote == "votedTest3") ? 1 : 0);

        //On attribue le vote à l'utilisateur à qui il appartient et à la proposition en cours
        $vote->setUser($user);
        $vote->setProposal($proposal);

        $entityManager->persist($vote);
        $entityManager->flush();
    }

}