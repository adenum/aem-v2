<?php

namespace TestVote\TestVoteBundle\Entity;

use App\Entity\Proposal;
use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VoteRepository")
 */
class VoteTest
{
	public function __construct()
        {
            $this->creationDate = new \DateTime('now');
        }
	
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var int L'identifiant dans la BDD
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Proposal", inversedBy="votes")
     * @ORM\JoinColumn(nullable=false)
     * @var Proposal La proposition concernée par le vote
     */
    private $proposal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="votes")
     * @ORM\JoinColumn(nullable=false)
     * @var User L'utilisateur votant
     */
	private $user;

    /**
     * @ORM\Column(type="date")
     * @var \DateTimeInterface La date du vote
     */
    private $creationDate;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": "0"})
     */
    private $votedTest1;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": "0"})
     */
    private $votedTest2;

    /**
     * @ORM\Column(type="boolean", nullable=false, options={"default": "0"})
     */
    private $votedTest3;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProposal(): ?Proposal
    {
        return $this->proposal;
    }

    public function setProposal(?Proposal $proposal): self
    {
        $this->proposal = $proposal;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVotedTest1()
    {
        return $this->votedTest1;
    }

    /**
     * @param mixed $votedTest1
     */
    public function setVotedTest1($votedTest1): void
    {
        $this->votedTest1 = $votedTest1;
    }

    /**
     * @return mixed
     */
    public function getVotedTest2()
    {
        return $this->votedTest2;
    }

    /**
     * @param mixed $votedTest2
     */
    public function setVotedTest2($votedTest2): void
    {
        $this->votedTest2 = $votedTest2;
    }

    /**
     * @return mixed
     */
    public function getVotedTest3()
    {
        return $this->votedTest3;
    }

    /**
     * @param mixed $votedTest3
     */
    public function setVotedTest3($votedTest3): void
    {
        $this->votedTest3 = $votedTest3;
    }
}
