<?php


namespace White\ThemeBundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use White\ThemeBundle\DependencyInjection\WhiteThemeExtension;

class WhiteThemeBundle extends Bundle
{
}