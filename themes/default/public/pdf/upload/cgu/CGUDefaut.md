# Charte des participants de la plateforme de prise de décision collective

## L'association ADENÜM

**ADENÜM** est une association de doit local Alsace Moselle à but non lucratif. Son but est de permettre à ses concitoyens de prendre en main les outils numériques mis à leur disposition, qu'ils soient libres ou propriétaires.

Nous favorisons cependant, lorsque cela est possible, les outils issus du monde informatique dit du *Libre*, et développons des outils dans cette optique, accessibles et utilisables par tous et gratuitement ou à coût moindre.

Vous avez demandé à rejoindre, en vous inscrivant sur la plateforme [democratie.mobile-adenum](https://democratie.mobile-adenum.fr/public), des citoyens qui, tout comme vous, visent à améliorer leur cadre de vie en débattant sur des sujets qui vous concernent, et à choisir les solutions qui conviendraient le mieux aux problèmes que vous vous posez.

## Fonctionnement

1. En vous connectant sur la plateforme, vous avez accès à un certain nombre de **Thèmes** relatifs aux débats.
2. Chaque **Thème** regroupe des **Ateliers** qui définissent le cadre précis dans lequel le débat doit se produire.
3. Dans chaque **Atelier**, vous pourrez faire des **Propositions** et discuter avec toutes les autres personnes inscrites sur et le sujet précis, défini par l'**Atelier**. Cette discussion se fait au travers d'un forum : chaque **Proposition** peut alors être discutée indépendamment.
4. À l'issue de cette phase de **Propositions** et de **Discussions** – ou dans le même temps lorsque les **Administrateurs** l'ont configuré ainsi –, vous pourrez alors exprimer votre **Vote** pour chacune des propositions faites. Cette phase se produit normalement après la phase de **Discussions**. Chaque vote est anonyme, même pour les **Administrateurs** ou les **Modérateurs** de la plateforme.
5. **Fait important** : si vous considérez que vous n'avez pas les compétences ou l'expertise requise pour vos **Votes**, vous pourrez **Déléguer** votre voix à toute personne inscrite aux mêmes **Thèmes** que vous.
6. À la fin du suffrage, il vous sera remis une **Restitution** des **Propositions** faites, et celles qui auront été retenues.
7. Un dispositif de **Quorum** permet également de savoir si un nombre suffisant de **Votes** a été atteint. Il est calculé sur la base du nombre d'inscrits au(x) **Thème(s)** et son pourcentage est défini par l'**Administrateur** dédié à ces **Thèmes**.

## Charte des Participants

En participant sur la plateforme [democratie.mobile-adenum](https://democratie.mobile-adenum.fr/public) aux débats qui vous préoccupent, vous vous engagez à :

1. Respecter toute expression de l'opinion d'une autre personne, que ce soit au travers d'une **Proposition** ou dans le cadre des **Discussions**. Dans les faits, cela signifie :
   * qu'il est possible d'être en opposition avec une **Proposition** ou l'expression d'une opinion dans le cadre des **Discussions**, mais qu'il n'est pas permis de les dénigrer ou d'insulter la personne qui les a exprimées
   * qu'il est interdit de critiquer le niveau d'expression d'une personne, que ce soit à cause de son écriture, des fautes qu'elle fait, etc.
   * qu'il est strictement interdit de critiquer une personne à cause de son âge, sexe, genre, origine, couleur de peau, ou de quelque caractère discriminant que ce soit : sur cette plateforme, nous discutons des idées et des façons de les mettre en œuvre, pas des personnes.
2. S'engager à faire des **Propositions** qui rentrent dans le cadre précis défini par l'**Atelier** dans lequel vous intervenez. Cela signifie :
   * d'être le plus précis possible dans la description de votre proposition
   * d'apporter, dans la mesure du possible, des solutions au problème posé
   * et, de façon générale, à permettre aux personnes avec qui vous participez à la consultation de pouvoir prendre position et de faire leur choix.

Ce sont des règles très simples qui seront vérifiées continuellement sur une base régulière par l'équipe des **Administrateurs** et des **Modérateurs**. Ils pourront vous proposer dans certains cas de modifier vos **Propositions** si elles ne leur semblent pas claires ou suffisamment précises.

### Que font les Administrateurs et les Modérateurs ?

* Ils peuvent eux-mêmes faire des **Propositions** et participer aux **Débats** et aux **Votes**. Seuls les **Administrateurs** généraux de la plateforme – qui s'occupent du fonctionnement global – n'y participent pas.
* Ils lisent régulièrement les participations, que ce soient des **Propositions** ou des **Discussions**.
* Ils peuvent, si ils considèrent que ces dernières contreviennent à la *Charte des Participants*, les effacer et s'engagent à prévenir les **Participants** que leurs **Propositions** ou un post inclus dans la **Discussion** ne conviennent pas à la *Charte des Participants*.
* Ils peuvent, dans le flux des **Discussions**, rappeler les règles de la présente *Charte* ou demander à ce qu'une **Proposition** soit modifiée si elle ne leur semble pas claire ou suffisamment précise
* Ils consultent également sur une base régulière l'email [info at mobile-adenum.fr](mailto:info@mobile-adenum.fr) : vous pouvez, via cet email, contacter les **Administrateurs** ou les modérateurs pour toute question ou remarque relative aux débats. Prière d'être concis et précis quant à votre **Signalement**. Ils vous répondront sur une base bi-hebdomadaire.