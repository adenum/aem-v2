<?php

namespace App\Controller;

use TestVote\TestVoteBundle\Entity\Website;
use App\Model\Theme;
use App\Service\PluginDenormalizer;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\PropertyInfo\Extractor\PhpDocExtractor;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;



class PluginThemesController extends AbstractController
{
    // Servant a convertir les json en objet plugin
    private PluginDenormalizer $pluginDenormalizer;
    public function __construct()
    {
        //initialisation des variable pour le denormalizer
        $extractor = new PropertyInfoExtractor([], [
            new PhpDocExtractor(),
            new ReflectionExtractor(),

        ]);

        $pluginDenormalizer = new PluginDenormalizer();
        $normalizers      = [
            $pluginDenormalizer,
            new ObjectNormalizer(null, null, null, $extractor),
            new ArrayDenormalizer(),

        ];
        $serializer       = new Serializer($normalizers, [new JsonEncoder()]);
        $pluginDenormalizer->setDenormalizer($serializer);
        $this->pluginDenormalizer = $pluginDenormalizer;
    }


    //Controller servant a recuperer tous les themes de notre applications et a les afficher
    /**
     * @Route("/admin/appearance", name="appearance_admin")
     * @throws ExceptionInterface
     */
    public function plugin(): Response
    {
        $finder = new Finder();
        //Récupération du répertoire courrant
        $file = getcwd();
        //Aller dans le répertoire themes
        $file .= '/../themes/';

        //Récuperation de tous les répertoire dans /themes avec un profondeurs de 0 (recupere que le premier répertoire)
        $finder->in($file)->depth(0)->directories();
        $themes = array();
        //Si on as récuperer des répertoire
        if($finder->hasResults()){
            //Parcours tous les repertoires
            foreach ($finder as $directorie){
                //Vérification de l'existance du fichier config.json
                if(file_exists($directorie->getRealPath()."\\config.json")){
                    //Récupération de notre config.json
                    $json = file_get_contents(
                        $directorie->getRealPath()."\\config.json");
                    //transformation de notre json en theme
                    $theme = $this->pluginDenormalizer->denormalize($json,'json');


                    //Si theme différent de null alors on l'ajoute a la liste des themes
                    if($theme != null) {
                        $themes[] = $theme;
                    }else{
                        $this->addFlash("danger","Theme : ".$directorie->getRealPath()." à un attribut fondamental manquant");
                    }
                }else{
                    $this->addFlash("danger","Theme : ".$directorie->getRealPath()." fichier config.json introuvable");
                }


            }
        }

        return $this->render("plugin/themes.html.twig",["themes" => $themes]);
    }


    /**
     * @Route("/image/{imagePath}", name="show_image")
     */
    //Fonction servant a l'affichage des images de prévisualisation
    public function showimageAction(string $imagePath): BinaryFileResponse
    {
        $response = new BinaryFileResponse(
            $this->getParameter('kernel.project_dir')
            ."\\themes\\"
            . $imagePath
            ."\\icon.png"
        );
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT
        );

        return $response;
    }

    /**
     * @Route("/admin/plugin/themes/change/{themePrefix}", name="change_background")
     * @throws ExceptionInterface
     */
    public function changeTheme(string $themePrefix, EntityManagerInterface $entityManager): RedirectResponse
    {
        $file =
            $this->getParameter('kernel.project_dir')
            ."/themes/"
            .$themePrefix;

        $json = file_get_contents(
            $file."\\config.json");
        $plugin = $this->pluginDenormalizer->denormalize($json, Theme::class);

        $this->clearProject($entityManager);
        //Sélection du type de Thème
        switch ($plugin->getSchema()){
            case 1:
                return  $this->themeSchema1($themePrefix,$entityManager);
                break;
            case 2:
                if($themePrefix != "default"){
                    return $this->themeSchema2($themePrefix,$entityManager);
                }
                break;
            default:
                $this->addFlash("danger","Schema inconnu!");
                return $this->redirectToRoute("appearance_admin");
        }
        return $this->redirectToRoute("appearance_admin");

    }



    private function themeSchema1(string $themePrefix, EntityManagerInterface $entityManager): RedirectResponse
    {
        //récupération de l'objet Website pour enregistrer le theme courrant
        $filesystem = new Filesystem();
        $finder = new Finder();
        $style = $this->getParameter('kernel.project_dir')."/themes/".$themePrefix."/css";

        $finder->files()->in($style);
        $firstTime = true;
        if($finder->hasResults()){
            foreach ($finder as $file) {
                $absoluteFilePath = $file->getRealPath();
                $fileNameWithExtension = $file->getRelativePathname();

                $cssWebsite = $this->getParameter('kernel.project_dir')."/public/css/".$fileNameWithExtension;

                if($filesystem->exists($cssWebsite)){
                    $filesystem->copy($absoluteFilePath, $cssWebsite, true);
                    if($firstTime){
                        $website = $this->getDoctrine()->getRepository(Website::class)->find(1);
                        $website->setBackgroundColor($themePrefix);
                        $entityManager->persist($website);
                        $this->getDoctrine()->getManager()->flush();
                        $this->get('session')->getFlashBag()->clear();
                        $this->addFlash("success","Themes changer.");
                        $firstTime=false;
                    }
                }
            }
            if($firstTime){
                $this->get('session')->getFlashBag()->clear();
                $this->addFlash('danger','Fichier thème incomplet');
            }
        }
        return $this->redirectToRoute("appearance_admin");

    }

    private function themeSchema2(string $themePrefix, EntityManagerInterface $entityManager): RedirectResponse
    {
        //récupération de l'objet Website pour enregistrer le theme courrant
        $website = $this->getDoctrine()->getRepository(Website::class)->find(1);
        $filesystem = new Filesystem();

        $style = $this->getParameter('kernel.project_dir')."/themes/".$themePrefix."/public";
        $style2 = $this->getParameter('kernel.project_dir')."/public";
        $template = $this->getParameter('kernel.project_dir')."/themes/".$themePrefix."/templates";
        $template2 = $this->getParameter('kernel.project_dir')."/templates";
        $translation = $this->getParameter('kernel.project_dir')."/themes/".$themePrefix."/translations";
        $translation2 = $this->getParameter('kernel.project_dir')."/translations";

        if($filesystem->exists($style)){
            $filesystem->mirror($style,$style2,null,['override' => true]);
        }

        if($filesystem->exists($template)){
            $filesystem->mirror($template,$template2,null,['override' => true]);
        }

        if($filesystem->exists($translation)){
            $filesystem->mirror($translation,$translation2,null,['override' => true]);
        }

        $website->setBackgroundColor($themePrefix);
        $entityManager->persist($website);
        $this->getDoctrine()->getManager()->flush();
        $this->get('session')->getFlashBag()->clear();
        $this->addFlash("success","Themes changer.");

        return $this->redirectToRoute("appearance_admin");

    }
    private function clearProject(EntityManagerInterface $entityManager){

        $filesystem = new Filesystem();

        $style = $this->getParameter('kernel.project_dir')."/themes/default/public";
        $style2 = $this->getParameter('kernel.project_dir')."/public";
        $template = $this->getParameter('kernel.project_dir')."/themes/default/templates";
        $template2 = $this->getParameter('kernel.project_dir')."/templates";
        $translation = $this->getParameter('kernel.project_dir')."/themes/default/translations";
        $translation2 = $this->getParameter('kernel.project_dir')."/translations";

        if($filesystem->exists($style)){
            $filesystem->mirror($style,$style2,null,['override' => true,'delete' => true]);
        }

        if($filesystem->exists($template)){
            $filesystem->mirror($template,$template2,null,['override' => true,'delete' => true]);
        }

        if($filesystem->exists($translation)){
            $filesystem->mirror($translation,$translation2,null,['override' => true,'delete' => true]);
        }
        $website = $this->getDoctrine()->getRepository(Website::class)->find(1);
        $website->setBackgroundColor("default");
        $this->addFlash("success","Themes changer.");
        $entityManager->persist($website);
        $this->getDoctrine()->getManager()->flush();
    }

}
