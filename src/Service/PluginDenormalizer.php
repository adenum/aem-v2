<?php

namespace App\Service;

use App\Model\Author;
use App\Model\Theme;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;


//Class servant a deserialiser le json des themes
class PluginDenormalizer implements DenormalizerInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    public function denormalize($data, string $type, string $format = null, array $context = []): ?Theme
    {
        $data = json_decode($data, true);

        //verifications que les fondamentaux sont bien renseigner
        if(!isset($data['prefix']) or !isset($data['version']) or !isset($data['etat']) or !isset($data['schema'])){
            return null;
        }

        //Ternaire controllant si les facultatif sont existant sinon il sont set a null
        $auteurs = isset($data['auteurs']) ? array_map(fn($author) => $this->denormalizer->denormalize($author, Author::class), $data['auteurs']) : null;
        $compatibilite = $data['compatibilite'] ?? null;
        $nom = $data['nom'] ?? null;
        $credit = $data['credit'] ?? null;
        $licence = $data['licence'] ?? null;

        return new Theme(
            $data['prefix'],
            $data['version'],
            $data['etat'],
            $compatibilite,
            $nom,
            $auteurs,
            $credit ,
            $licence,
            $data['schema'],
        );
    }

    public function supportsDenormalization($data, string $type, string $format = null): bool
    {
        return $type === Theme::class;
    }
}