<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211104044940 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE category CHANGE name name VARCHAR(127) NOT NULL');
        $this->addSql('ALTER TABLE keyword CHANGE name name VARCHAR(127) NOT NULL');
        $this->addSql('ALTER TABLE notification CHANGE subject subject MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE proposal CHANGE name name VARCHAR(127) NOT NULL');
        $this->addSql('ALTER TABLE theme CHANGE name name VARCHAR(127) NOT NULL, CHANGE description description MEDIUMTEXT NOT NULL, CHANGE vote_type vote_type VARCHAR(127) NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE email email VARCHAR(127) NOT NULL');
        $this->addSql('ALTER TABLE vote ADD voted_full_agreement TINYINT(1) DEFAULT NULL, ADD voted_agree TINYINT(1) DEFAULT NULL, ADD voted_mixed TINYINT(1) DEFAULT NULL, ADD voted_disagree TINYINT(1) DEFAULT NULL, ADD voted_total_disagreement TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE website CHANGE url url VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE workshop CHANGE description description MEDIUMTEXT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE category CHANGE name name VARCHAR(40) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE keyword CHANGE name name VARCHAR(40) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE notification CHANGE subject subject MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE proposal CHANGE name name VARCHAR(40) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE theme CHANGE name name VARCHAR(40) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE description description MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE vote_type vote_type VARCHAR(40) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE user CHANGE email email VARCHAR(40) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE vote DROP voted_full_agreement, DROP voted_agree, DROP voted_mixed, DROP voted_disagree, DROP voted_total_disagreement');
        $this->addSql('ALTER TABLE website CHANGE url url VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE workshop CHANGE description description MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}
