<?php


namespace App\Form;


use TestVote\TestVoteBundle\Entity\Website;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Liip\ImagineBundle\Form\Type\ImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;
use function Sodium\add;

class WebsiteType extends AbstractType
{
    /**
     * L'administrateur peut modifier le nom, la version, l'auteur et l'email administrateur de l'application.
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class,[
                'label' => 'name'
            ])
            ->add('Version', TextType::class,[

            ])
            ->add('title', TextType::class,[
                'label' => 'title'
            ])
            ->add('url', TextType::class,[
                'action'=>UrlType::class,
                'required'=>false
            ])

            ->add('logoFile', VichImageType::class, [
                'allow_delete' => true,
                'required' => false,
                'label' => 'logo'


            ])
            ->add('email', EmailType::class)
            ->add('loginMessage', CKEditorType::class, [
                'label'=>'login_message',
                'config' => [
                    'toolbar' => 'full',
                ],
            ])
            ->add('registrationMessage', CKEditorType::class, [
                'label'=>'registration_message',
                'config' => [
                    'toolbar' => 'full',
                ],
            ])
            #Ajout d'un champs pour le message de mail par defaut apres inscription
            ->add('mailRegistration', CKEditorType::class, [
                'label'=>'mail.registration',
                'config' => [
                    'toolbar' => 'full',
                    ],
            ])
            ->add('imageFile', VichImageType::class, [
                'label'=>'image_file',
                'required' => false,
                'allow_delete' => true,
                'help'=>'background_img',
            ])
            ->add('backgroundColor', ColorType::class, [
                'label' => 'background'
            ])
            ->add('indexing', CheckboxType::class, [
                'label' => 'website.index',
                'required' => false
            ])
            ->add('Submit', SubmitType::class,[
                'label' => 'submit'
            ])
       ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Website::class
        ]);
    }

}