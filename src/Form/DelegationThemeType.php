<?php
namespace App\Form;

use TestVote\TestVoteBundle\Entity\Category;
use TestVote\TestVoteBundle\Entity\Delegation;
use TestVote\TestVoteBundle\Entity\Theme;
use TestVote\TestVoteBundle\Entity\User;
use App\Repository\CategoryRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use \Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DelegationThemeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
    $id = $options['categoryId'];
		$builder

				->add('userTo', EntityType::class, [
                    'label'=>'userTo',
					'class'			 => User::class,
                    'choice_label'	 => 'username',
                    'multiple' => false,
                    'choices' => $id->getUsers()

])

				->add('Submit', SubmitType::class,['label'=>'submit'])
		;
	}
	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults([
			'data_class' => Delegation::class,
		]);
        $resolver->setRequired(['categoryId']);
	}

}