<?php

namespace App\Model;

class Theme
{
    private $prefix;
    private $version;
    private $etat;
    private $compatibilite;
    private $nom;
    /**
     * @var Author[] $auteurs
     */
    private ?array $auteurs;
    private $credit;
    private $licence;
    private $schema;

    /**
     * @param $prefix
     * @param $version
     * @param $etat
     * @param $compatibilite
     * @param $nom
     * @param $auteur
     * @param $credit
     * @param $licence
     * @param $schema
     */
    public function __construct($prefix, $version, $etat, $compatibilite, $nom, $auteur, $credit, $licence, $schema)
    {
        $this->prefix = $prefix;
        $this->version = $version;
        $this->etat = $etat;
        $this->compatibilite = $compatibilite;
        $this->nom = $nom;
        $this->auteurs = $auteur;
        $this->credit = $credit;
        $this->licence = $licence;
        $this->schema = $schema;
    }


    /**
     * @return mixed
     */
    public function getSchema()
    {
        return $this->schema;
    }

    /**
     * @param mixed $schema
     */
    public function setSchema($schema): void
    {
        $this->schema = $schema;
    }

    /**
     * @return mixed
     */
    public function getPrefix()
    {
        return $this->prefix;
    }

    /**
     * @param mixed $prefix
     */
    public function setPrefix($prefix): void
    {
        $this->prefix = $prefix;
    }

    /**
     * @return mixed
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param mixed $version
     */
    public function setVersion($version): void
    {
        $this->version = $version;
    }

    /**
     * @return mixed
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @param mixed $etat
     */
    public function setEtat($etat): void
    {
        $this->etat = $etat;
    }

    /**
     * @return mixed
     */
    public function getCompatibilite()
    {
        return $this->compatibilite;
    }

    /**
     * @param mixed $compatibilite
     */
    public function setCompatibilite($compatibilite): void
    {
        $this->compatibilite = $compatibilite;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom): void
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getAuteurs()
    {
        return $this->auteurs;
    }

    /**
     * @param ?Author $auteurs
     */
    public function setAuteurs(?Author $auteurs): void
    {

        $this->auteurs[] = $auteurs;
    }

    /**
     * @return mixed
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * @param mixed $credit
     */
    public function setCredit($credit): void
    {
        $this->credit = $credit;
    }

    /**
     * @return mixed
     */
    public function getLicence()
    {
        return $this->licence;
    }

    /**
     * @param mixed $licence
     */
    public function setLicence($licence): void
    {
        $this->licence = $licence;
    }




}