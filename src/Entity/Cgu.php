<?php

namespace App\Entity;

use App\Repository\CguRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CguRepository::class)
 */
class Cgu
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pdfFilename;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="cgus")
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity=User::class, mappedBy="cguAccepted")
     */
    private $usersAccepted;

    public function __construct()
    {
        $this->usersAccepted = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }








    public function getPdfFilename(): ?string
    {
        return $this->pdfFilename;
    }

    public function setPdfFilename(?string $pdfFilename): self
    {
        $this->pdfFilename = $pdfFilename;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsersAccepted(): Collection
    {
        return $this->usersAccepted;
    }

    public function addUsersAccepted(User $usersAccepted): self
    {
        if (!$this->usersAccepted->contains($usersAccepted)) {
            $this->usersAccepted[] = $usersAccepted;
            $usersAccepted->addCguAccepted($this);
        }

        return $this;
    }

    public function removeUsersAccepted(User $usersAccepted): self
    {
        if ($this->usersAccepted->removeElement($usersAccepted)) {
            $usersAccepted->removeCguAccepted($this);
        }

        return $this;
    }
}
